// Task: Implement a struct named 'RangeList'
// A pair of integers define a range, for example: [1, 5). This range includes integers: 1, 2, 3, and 4.
// A range list is an aggregate of these ranges: [1, 5), [10, 11), [100, 201)
// NOTE: Feel free to add any extra member variables/functions you like.
package main

import "fmt"

type RangeList struct {
	items [][2]int
}

func (rangeList *RangeList) Add(rangeElement [2]int) error {
	// Add思路: 添加的内容, 除了[0,0), 肯定是要增加一个元素进来, 但如果完全重合, 则不需要添加元素, 这样处理比较麻烦
	// 所以, 改成无条件增加一个元素进来, 删除完全覆盖的元素, 吸收部分重合的元素, 延长到添加元素中, 用一个替换若干个
	// 最终还是必添加一个元素, 删除0到任意个元素
	// 设置一个删除区间[delIndexStart, delIndexStart)
	// 设置一个插入位置 insertPosition
	// 即可

	if len(rangeList.items) == 0 {
		rangeList.items = append(rangeList.items, rangeElement)
		return nil
	}
	start := rangeElement[0]
	end := rangeElement[1]
	// Fix 0 length case
	if end-start == 0 {
		return nil
	}

	// Delete range of index, like [0, 1)
	delIndexStart := 0
	delIndexEnd := 0

	// Insert position only use when nothing to delete, -1 means first position of index
	insertPosition := -1
	needInsert := true

	for index, item := range rangeList.items {
		itemLeft := item[0]
		itemRight := item[1]

		// If not in range, update insertPosition and continue
		if itemRight < start {
			insertPosition = index
			continue
		} else if itemLeft <= start && start <= itemRight { // If start point in range,
			// And end point in range too, do nothing
			if itemRight >= end {
				needInsert = false
				break
			}
			// If not, append this array
			if delIndexStart == 0 && delIndexEnd == 0 {
				delIndexStart = index
				delIndexEnd = index + 1
			} else {
				delIndexEnd = index + 1
			}
			rangeElement = [2]int{itemLeft, end}
			start = itemLeft
		} else { // start < itemLeft
			// If start & end both less than range left, it's need to break
			if end < itemLeft {
				break
			}

			// If end can cover part of range, expand it.
			if itemLeft <= end {
				if delIndexStart == 0 && delIndexEnd == 0 {
					delIndexStart = index
					delIndexEnd = index + 1
				} else {
					delIndexEnd = index + 1
				}
				if end < itemRight {
					rangeElement = [2]int{start, itemRight}
				} else {
					rangeElement = [2]int{start, end}
				}
				// if end less than range right, means it's end, break
				if end <= itemRight {
					break
				}
				end = itemRight
			}
		}
	}

	// if only insert new list in position
	if delIndexStart == 0 && delIndexEnd == 0 {
		if insertPosition == -1 { // If set to first
			if needInsert {
				newRangeList := [][2]int{rangeElement}
				rangeList.items = append(newRangeList, rangeList.items...)
			}
		} else {
			frontRange := rangeList.items[0 : insertPosition+1]
			backRange := rangeList.items[insertPosition+1:]
			rangeList.items = append(frontRange, rangeElement)
			rangeList.items = append(rangeList.items, backRange...)
		}
	} else {
		if delIndexStart == 0 { // If set to first
			newRangeList := [][2]int{rangeElement}
			rangeList.items = append(newRangeList, rangeList.items[delIndexEnd:]...)
		} else if delIndexEnd == len(rangeList.items) { // If set to end
			rangeList.items = append(rangeList.items[:delIndexStart], rangeElement)
		} else { // If set to middle
			frontRange := rangeList.items[0:delIndexStart]
			backRange := rangeList.items[delIndexEnd:]
			rangeList.items = append(frontRange, rangeElement)
			rangeList.items = append(rangeList.items, backRange...)
		}
	}
	return nil
}
func (rangeList *RangeList) Remove(rangeElement [2]int) error {
	// Delete思路:
	// case1: rangeElement把原来的区间一分为二
	// case2: rangeElement把某一个区间砍掉一部分,后面的砍掉若干个完整的, 最后一个区间砍掉一部分或整个砍掉, 还留有一部分
	// case3: rangeElement把全部范围的区间全部砍掉
	// 总的来看只有case1可能会导致区间数量增加一个, 所以设置了一个insertPosition
	// 其他情况都是删除若干, 头尾砍掉一部分或不动, 所以头尾直接当时就修改, 删除的部分记录下来, 最后一起删
	if len(rangeList.items) == 0 {
		return nil
	}
	start := rangeElement[0]
	end := rangeElement[1]
	// Fix 0 length case
	if end-start == 0 {
		return nil
	}

	// Delete range of index, like [0, 1)
	delIndexStart := 0
	delIndexEnd := 0

	// Insert position only use when nothing to delete, -1 means first position of index
	insertPosition := -1
	var insertItem [2]int

	for index, item := range rangeList.items {
		itemLeft := item[0]
		itemRight := item[1]

		// If not in range, update insertPosition and continue
		if itemRight < start {
			continue
		} else if itemLeft < start && start < itemRight { // If start point in range,
			// update range front
			rangeList.items[index] = [2]int{itemLeft, start}
			// And end point in range too, cut this to two pieces, and break
			if itemRight > end {
				// save range back
				insertPosition = index
				insertItem = [2]int{end, itemRight}
				break
			}
		} else { // start <= itemLeft
			// If start & end both less than range left, it's need to break
			if end < itemLeft {
				break
			}

			// If end can cover part of range, expand it.
			if itemLeft <= end {
				if end < itemRight {
					rangeList.items[index] = [2]int{end, itemRight}
					break
				}
				if delIndexStart == 0 && delIndexEnd == 0 {
					delIndexStart = index
					delIndexEnd = index + 1
				} else {
					delIndexEnd = index + 1
				}
				// if end less than range right, means it's end, break
				if end == itemRight {
					break
				}
			}
		}
	}

	// if only insert new list in position
	if delIndexStart != 0 || delIndexEnd != 0 {
		// All delete case
		if delIndexStart == 0 && delIndexEnd == len(rangeList.items) {
			rangeList.items = make([][2]int, 0)
			return nil
		}
		if delIndexStart == 0 { // Delete from first to middle
			rangeList.items = rangeList.items[delIndexEnd:]
		} else if delIndexEnd == len(rangeList.items) { // Delete from middle to last
			rangeList.items = rangeList.items[:delIndexStart]
		} else { // Delete from middle to middle
			frontRange := rangeList.items[0:delIndexStart]
			backRange := rangeList.items[delIndexEnd:]
			rangeList.items = append(frontRange, backRange...)
		}
	}

	if insertPosition != -1 {
		frontRange := rangeList.items[0 : insertPosition+1]
		backRange := rangeList.items[insertPosition+1:]
		rangeList.items = append(frontRange, insertItem)
		rangeList.items = append(rangeList.items, backRange...)
	}

	return nil
}
func (rangeList *RangeList) Print() error {
	if len(rangeList.items) == 0 {
		fmt.Printf("Empty list.")
		return nil
	}

	for _, item := range rangeList.items {
		fmt.Printf("[%d, %d) ", item[0], item[1])
	}
	fmt.Println()
	return nil
}

func main() {
	rl := RangeList{}
	rl.Add([2]int{1, 5})
	rl.Print()
	// Should display: [1, 5)
	rl.Add([2]int{10, 20})
	rl.Print()
	// Should display: [1, 5) [10, 20)
	rl.Add([2]int{20, 20})
	rl.Print()
	// Should display: [1, 5) [10, 20)
	rl.Add([2]int{20, 21})
	rl.Print()
	// Should display: [1, 5) [10, 21)
	rl.Add([2]int{2, 4})
	rl.Print()
	// Should display: [1, 5) [10, 21)
	rl.Add([2]int{3, 8})
	rl.Print()
	// Should display: [1, 8) [10, 21)
	rl.Remove([2]int{10, 10})
	rl.Print()
	// Should display: [1, 8) [10, 21)
	rl.Remove([2]int{10, 11})
	rl.Print()
	// Should display: [1, 8) [11, 21)
	rl.Remove([2]int{15, 17})
	rl.Print()
	// Should display: [1, 8) [11, 15) [17, 21)
	rl.Remove([2]int{3, 19})
	rl.Print()
	// Should display: [1, 3) [19, 21)
	rl.Remove([2]int{0, 10})
	rl.Print()
	// Should display: [19, 21)
	rl.Add([2]int{1, 3})
	rl.Print()
	// Should display: [1, 3) [19, 21)
	rl.Add([2]int{3, 19})
	rl.Print()
	// Should display: [1, 21)
	rl.Add([2]int{30, 40})
	rl.Print()
	// Should display: [1, 21) [30, 40)
	rl.Add([2]int{20, 60})
	rl.Print()
	// Should display: [1, 60)
	rl.Remove([2]int{0, 100})
	rl.Print()
	// Should display:

}
